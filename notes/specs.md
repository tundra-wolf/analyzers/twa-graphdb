# Graph Databases

##  Neo4J

The Cypher query language is documented [here](https://neo4j.com/docs/cypher-manual/current/introduction/).

## GQL

## GraphQL

The latest version of GraphQL is at the [GraphQL.org website](https://spec.graphql.org/draft/).
